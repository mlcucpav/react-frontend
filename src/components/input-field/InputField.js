import axios from "axios";
import { useCallback, useState } from "react";

const InputField = props => {

    const [ message, setMessage ] = useState(null);
    const { validation, label, onValidation, className, object, property, ...filteredProps } = props;

    const httpValidation = useCallback(async (http, value) => {
        try {
             //            axios.get(`/users?email=${value}`)
            const result = await axios[http.method](`${http.url}${value}`)
            //                                                        result.data = [{name: 'Pavel'}] ==> ! ==> false ==> !! true
            return http.resolver ? http.resolver(result.data, null) : !!result.data
        } catch (err) {
            return http.resolver ? http.resolver(null, err) : false;
        }
    }, []);

    const validate = useCallback(async event => {
        const messages = [];
        const value = event.target.value;
        if (validation && validation.required && !value) {
            messages.push(`Field ${label} is required!`);
        }
        if (validation && validation.minLength && (!value || value.length < validation.minLength)) {
            messages.push(`Field ${label} must have at least ${validation.minLength} characters!`)
        }
        if (validation && validation.regex && !validation.regex.test(value)) {
            messages.push(validation.message || `Field ${label} failed validation!`);
        }
        if (validation && validation.http && validation.http.trigger(object) && !await httpValidation(validation.http, value)) {
            messages.push(validation.http.message);
        }
        setMessage(messages.join(' '));
        onValidation(label, messages.length === 0);
    }, [validation, label, onValidation, httpValidation]);

    const value = props.object[props.property];

    const inputProps = {
        ...filteredProps,
        value: value || '',
        onBlur: validate
    };

    return <div className={props.className}>
        <div>
            <label htmlFor={props.label}>{props.label}</label>
        </div>
        <div>
            <input {...inputProps} />
        </div>
        {message && <div style={{color: 'red', fontSize: '66%'}}>{message}</div>}
    </div>
};

export default InputField;