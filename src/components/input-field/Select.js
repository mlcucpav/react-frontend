import { useCallback, useEffect, useState } from "react";

const Select = props => {

    const { label, multiple, values, setValues, propertyObjectValueProperty, touched, validation, onValidation, options } = props;
    const [message, setMessage] = useState(null);

    const validate = useCallback(() => {
        if (touched) {
            const messages = [];
            if (validation && validation.min && values.length < validation.min) {
                messages.push(`At least ${validation.min} option${validation.min > 1 ? 's' : ''} must be selected!`);
            }
            if (validation && validation.max && values.length > validation.max) {
                messages.push(`Maximum of ${validation.max} options allowed!`);
            }
            setMessage(messages.join(' '));
            onValidation(label, messages.length === 0);
        }
    }, [validation, label, onValidation, values, touched]);

    useEffect(validate, [values, validate]);

    const handleSelectChange = useCallback(event => {
        const selectedOptions = Array.from(event.target.selectedOptions);
        const selectedValues = selectedOptions.map(option => ({[propertyObjectValueProperty]: option.value}))
        setValues(selectedValues);
    }, [setValues, propertyObjectValueProperty]);

    return <div className={props.className}>
        <div>
            <label htmlFor={label}>{label}</label>
        </div>
        <div>
            <select id={label} 
                    multiple={multiple} 
                    onChange={handleSelectChange}
                    onBlur={validate}>
                {options.map(op => <option key={op.value} value={op.value} selected={values && values.find(val => op.value === val)}>{op.label}</option>)}
            </select>
        </div>
        {message && <div style={{color: 'red', fontSize: '66%'}}>{message}</div>}
    </div>;
}
export default Select;