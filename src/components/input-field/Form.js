import { useCallback, useEffect, useMemo, useState } from "react";
import InputField from "./InputField";
import Select from "./Select";

const Form = props => {

    const { form, value, buttonLabel, onSubmmit, edit } = props;

    // Object.entries: { label: newField ...} ==> [[label, newField], ...]
    // Object.fromEntries: [[label, newField], [label, newField], [label, newField]] ==> { label: newField ...} 
    const enhancedForm = useMemo(() => Object.fromEntries(Object.entries(form).map(arr => {
        const [label, field] = arr;
        const initialyValid = field.type === 'password' ? false : !!edit;
        const newField = {
            ...field,
            valid: initialyValid,
            pristine: true,
            touched: false,
            originalValue: value[field.property]
        };
        return [label, newField];
    })), [form, value, edit]);

    const [object, setObject] = useState(value);
    const [valid, setValid] = useState(false);

    useEffect(() => {
        setObject(value);
        setValid(false);
    }, [value])

    const onFieldValidation = useCallback((label, valid) => {
        enhancedForm[label].valid = valid;
        enhancedForm[label].pristine = object[enhancedForm[label].property] === enhancedForm[label].originalValue;
        enhancedForm[label].touched = true;
        const formValid = !Object.entries(enhancedForm).find(arr => arr[1].valid === false);
        setValid(formValid);
    }, [enhancedForm, object]);

    const setSelectValues = useCallback((label, property, values) => {
        // { name: '...', roles: []}
        setObject({
            ...object,
            [property]: values // roles: [{name: 'HR'}] 
        });
        // { name: '...', roles: [{name: 'HR'}]}
        enhancedForm[label].touched = true;
    }, [object, enhancedForm]);

    const rendered = Object.entries(enhancedForm).map(arr => {
        const [label, field] = arr;
        if (field.type === 'select') {
            return <Select
                key={label}
                label={label}
                multiple={field.multiple}
                options={field.options}
                values={object[field.property].map(item => item[field.propertyObjectValueProperty])} // ['MANAGER', 'HR']
                setValues={values => setSelectValues(label, field.property, values)}
                propertyObjectValueProperty={field.propertyObjectValueProperty}
                validation={field.validation}
                onValidation={onFieldValidation}
                touched={field.touched} />
        } else {
            return <InputField 
                key={label}
                className="TableRow"
                label={label}
                object={object}
                property={field.property}
                onChange={e => setObject({...object, [field.property]: e.target.value})}
                id={field.id}
                type={field.type}
                validation={field.validation}
                onValidation={onFieldValidation} />
        }
    });

    return <div>
        {rendered}
        <button disabled={!valid} onClick={() => onSubmmit(object)}>{buttonLabel}</button>
    </div>;
};

export default Form;