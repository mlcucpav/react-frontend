import { useSelector } from 'react-redux';
import './UserList.css';

const UserList = props => {

    const users = useSelector(state => state.usersState.userList);

    return <div className="UsersTable">
        <div>
            <div>Name</div>
            <div>Email</div>
            <div>Roles</div>
            <div>&nbsp;</div>
        </div>
        {users.map(u => <div key={u.id}>
            <div>{u.name}</div>
            <div>{u.email}</div>
            <div>{u.roles.map(r => r.name).join(', ')}</div>
            <div>
                <button onClick={() => props.editUser(u)}>Edit</button>
                <button onClick={() => props.deleteUser(u)}>Delete</button>
            </div>
        </div>)}
    </div>;
};

export default UserList;