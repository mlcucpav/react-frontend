import { useSelector } from "react-redux";
import Form from "../../input-field/Form";

const roles = [
    { value: 'HR', label: 'HR' },
    { value: 'MANAGER', label: 'Manager' },
    { value: 'SPECIALIST', label: 'Specialist' },
]

const form = {
    'Name': {
        property: 'name',
        validation: {
            required: true
        }
    },
    'Email': {
        property: 'email',
        type: 'email',
        validation: {
            regex: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
            http: {
                url: '/users?email=',
                method: 'get',
                message: 'User with this email already exists',
                resolver: (data, _) => !(data && data.length)
            }
        }
    },
    'Password': {
        property: 'password',
        id: 'Password',
        type: 'password',
        validation: {
            minLength: 7
        }
    },
    'Roles': {
        property: 'roles',
        type: 'select',
        multiple: true,
        options: roles,
        propertyObjectValueProperty: 'name',
        validation: {
            min: 2,
            max: 2
        }
    }
};

const UserForm = props => {

    const user = useSelector(state => state.usersState.userForEdit);
    const buttonLabel = useSelector(state => state.uiState.buttonLabel);

    return <div style={{paddingLeft: '15px'}}>
        <Form form={form} value={user} edit={!!user.id} onSubmmit={props.handleButtonClick} buttonLabel={buttonLabel} />
    </div>;
};
export default UserForm;