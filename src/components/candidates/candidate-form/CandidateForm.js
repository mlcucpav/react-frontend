import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {  useRouteMatch } from "react-router-dom";
import { startCandidateUpdate } from "../../../store/CandidateActions";
import Form from '../../input-field/Form';

const form = {
    Name: {
        property: 'name',
        validation: {
            required: true
        }
    },
    Email: {
        property: 'email',
        type: 'email',
        validation: {
            regex: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
            http: {
                trigger: data => !data.id,
                url: '/candidates?email=',
                method: 'get',
                message: 'Candidate with this email already exists',
                resolver: (data, _) => !(data && data.length)
            }
        }
    },
    Phone: {
        property: 'phone',
        validation: {
            regex: /^\+\d{1,4} [\d ]{6,12}$/,
            message: 'Phone must be in following format: +420 123456789'
        }
    }
};

const CandidateForm = props => {

    const { params: { candidateId } } = useRouteMatch();
    const id = Number(candidateId);

    const dispatch = useDispatch();
    const buttonLabel = useSelector(state => state.uiState.buttonLabel);
    const message = useSelector(state => state.uiState.error);
    const candidate = useSelector(state => state.candidatesState.candidateForEdit);

    useEffect(() => id && startCandidateUpdate(dispatch, id), [id, dispatch]);

    return <div style={{paddingLeft: '15px'}}>
        {message && <div style={{color: 'red'}}><h2>{message}</h2></div>}
        {candidate && <Form form={form} value={candidate} edit={!!candidate.id} onSubmmit={props.handleButtonClick} buttonLabel={buttonLabel} />}
    </div>;
};
export default CandidateForm;