import { useSelector } from 'react-redux';
import { Link, useRouteMatch } from 'react-router-dom';
import './CandidateList.css';

const CandidateList = props => {

    const match = useRouteMatch();

    const candidates = useSelector(state => state.candidatesState.candidateList);

    return <div className="UsersTable">
        <div>
            <div>Name</div>
            <div>Email</div>
            <div>Phone</div>
            <div>&nbsp;</div>
        </div>
        {candidates.map(u => <div key={u.id}>
            <div>{u.name}</div>
            <div>{u.email}</div>
            <div>{u.phone}</div>
            <div>
                <Link to={`${match.url}/${u.id}`}>Edit</Link>
                <button onClick={() => props.delete(u)}>Delete</button>
            </div>
        </div>)}
    </div>;
};

export default CandidateList;