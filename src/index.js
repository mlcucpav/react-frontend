import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import UserReducer from './store/UserReducer';
import UIReducer from './store/UIReducer';
import CandidateReducer from './store/CandidateReducer';

const rootReducers = combineReducers({
  usersState: UserReducer,
  uiState: UIReducer,
  candidatesState: CandidateReducer
});

const store = createStore(rootReducers);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

