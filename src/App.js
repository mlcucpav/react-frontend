import './App.css';
import axios from "axios";
import UsersPage from './pages/UsersPage';
import { 
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import CandidatesPage from './pages/CandidatesPage';

axios.defaults.baseURL = 'http://localhost:8080/api/v1';
axios.defaults.headers.common['Authorization'] = 'Basic cGF2ZWwubWxjdWNoQHRpZXRvZXZyeS5jb206c3VwZXItc2VjcmV0';
axios.defaults.headers.post['Content-Type'] = 'application/json';

function App() {
  return (
    <Router>
      <div className="App">
        <div className="Navigation">
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/users">Users</Link>
              </li>
              <li>
                <Link to="/candidates">Candidates</Link>
              </li>
            </ul>
          </nav>
        </div>
        <div className="Content">
          <Switch>
            <Route path="/users">
              <UsersPage />
            </Route>
            <Route path="/candidates">
              <CandidatesPage />
            </Route>
            <Route path="/">
              <div>
                <h1>This is home page!</h1>
              </div>
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
