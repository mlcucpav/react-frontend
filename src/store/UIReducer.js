import { ADDED_CANDIDATE, ADD_CANDIDATE, ADD_CANDIDATE_ERROR, DELETED_CANDIDATE, DELETE_CANDIDATE, DELETE_CANDIDATE_ERROR, LOADED_CANDIDATES, LOAD_CANDIDATES, LOAD_CANDIDATES_ERROR, START_CANDIDATE_UPDATE, UPDATED_CANDIDATE, UPDATE_CANDIDATE, UPDATE_CANDIDATE_ERROR } from "./CandidateActions";
import { ADDED_USER, ADD_USER, ADD_USER_ERROR, DELETED_USER, DELETE_USER, DELETE_USER_ERROR, LOADED_USERS, LOAD_USERS, LOAD_USERS_ERROR, START_USER_UPDATE, UPDATED_USER, UPDATE_USER, UPDATE_USER_ERROR } from "./UserActions";

const initialState = {
    loading: false,
    error: null,
    buttonLabel: 'Add'
};

const UIReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_USER:
        case ADD_CANDIDATE:
        case LOAD_USERS: 
        case LOAD_CANDIDATES: 
        case UPDATE_USER:
        case UPDATE_CANDIDATE:
        case DELETE_USER:
        case DELETE_CANDIDATE:
            return { ...state, loading: true, error: null };
        case ADDED_USER:
        case ADDED_CANDIDATE:
        case LOADED_USERS:
        case LOADED_CANDIDATES:
        case DELETED_USER:
        case DELETED_CANDIDATE:
            return { ...state, loading: false };
        case UPDATED_USER:
        case UPDATED_CANDIDATE:
            return { ...state, loading: false, buttonLabel: initialState.buttonLabel };
        case ADD_USER_ERROR:
        case ADD_CANDIDATE_ERROR:
        case LOAD_USERS_ERROR: 
        case LOAD_CANDIDATES_ERROR: 
        case UPDATE_USER_ERROR:
        case UPDATE_CANDIDATE_ERROR:
        case DELETE_USER_ERROR:
        case DELETE_CANDIDATE_ERROR:
            return { ...state, loading: false, error: action.payload };
        case START_USER_UPDATE:
        case START_CANDIDATE_UPDATE:
            return { ...state, buttonLabel: 'Save' };
        default:
            return state;
    }
};

export default UIReducer