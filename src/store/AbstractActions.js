import axios from "axios";

export const add = async (dispatch, object, urlPart, [addStart, addEnd, addError]) => {
    dispatch({ type: addStart });
    try {
        const response = await axios.post(urlPart, object);
        dispatch({ type: addEnd, payload: response.data });
    } catch (err) {
        dispatch({ type: addError, payload: extractError(err) });
    }
};

export const extractError = err => err.response ? err.response.data.message : err;