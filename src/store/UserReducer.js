import { ADDED_USER, DELETED_USER, LOADED_USERS, START_USER_UPDATE, UPDATED_USER } from "./UserActions";

const initialState = {
    userList: [],
    userForEdit: {
        name: '',
        email: '',
        password: '',
        roles: []
    }
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOADED_USERS:
            return { ...state, userList: action.payload };
        case ADDED_USER:
            return { ...state, 
                userList: [...state.userList, action.payload], 
                userForEdit: { ...initialState.userForEdit }
            };
        case START_USER_UPDATE: 
            return { ...state, userForEdit: action.payload };
        case UPDATED_USER: {
            const newList = [...state.userList].filter(u => u.id !== action.payload.id);
            return { ...state, 
                userList: [...newList, action.payload], 
                userForEdit: initialState.userForEdit 
            };
        }
        case DELETED_USER: {
            const newList = [...state.userList].filter(u => u.id !== action.payload); 
            return { ...state, userList: newList };
       }
        default:
            return state;
    }
};

export default UserReducer;