import { ADDED_CANDIDATE, DELETED_CANDIDATE, LOADED_CANDIDATES, START_CANDIDATE_ADD, START_CANDIDATE_UPDATE, UPDATED_CANDIDATE } from "./CandidateActions";

const initialState = {
    candidateList: [],
    candidateForEdit: null
};
const emptyCandidateForEdit = {
    name: '',
    email: '',
    phone: ''
}

const CandidateReducer = (state = initialState, action) => {
    switch (action.type) {
        case START_CANDIDATE_ADD:
            return { ...state, candidateForEdit: emptyCandidateForEdit };
        case LOADED_CANDIDATES:
            return { ...state, candidateList: action.payload };
        case ADDED_CANDIDATE:
            return { ...state, 
                candidateList: [...state.candidateList, action.payload], 
                candidateForEdit: null
            };
        case START_CANDIDATE_UPDATE: 
            const candidateForEdit = state.candidateList.find(c => c.id === action.payload);
            return { ...state, candidateForEdit: candidateForEdit };
        case UPDATED_CANDIDATE: {
            const newList = [...state.candidateList].filter(u => u.id !== action.payload.id);
            return { ...state, 
                candidateList: [...newList, action.payload], 
                candidateForEdit: null
            };
        }
        case DELETED_CANDIDATE: {
            const newList = [...state.candidateList].filter(u => u.id !== action.payload); 
            return { ...state, candidateList: newList };
       }
        default:
            return state;
    }
};

export default CandidateReducer;