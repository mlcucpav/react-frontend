import axios from "axios";
import { add, extractError } from "./AbstractActions";

export const LOAD_CANDIDATES = 'LOAD_CANDIDATES';
export const LOADED_CANDIDATES = 'LOADED_CANDIDATES';
export const LOAD_CANDIDATES_ERROR = 'LOAD_CANDIDATES_ERROR';

export const loadCandidates = async dispatch => {
    dispatch({ type: LOAD_CANDIDATES });
    try {
        const response = await axios.get('/candidates');
        dispatch({ type: LOADED_CANDIDATES, payload: response.data });
    } catch (err) {
        dispatch({ type: LOAD_CANDIDATES_ERROR, payload: extractError(err) })
    }
};

export const START_CANDIDATE_ADD = 'START_CANDIDATE_ADD';

export const startCandidateAdd = dispatch => {
    dispatch({ type: START_CANDIDATE_ADD });
};

export const ADD_CANDIDATE = 'ADD_CANDIDATE';
export const ADDED_CANDIDATE = 'ADDED_CANDIDATE';
export const ADD_CANDIDATE_ERROR = 'ADD_CANDIDATE_ERROR';

export const addCandidate = async (dispatch, candidate) => {
    add(dispatch, candidate, '/candidates', [ADD_CANDIDATE, ADDED_CANDIDATE, ADD_CANDIDATE_ERROR])
};

export const START_CANDIDATE_UPDATE = 'START_CANDIDATE_UPDATE';

export const startCandidateUpdate = (dispatch, id) => {
    dispatch({ type: START_CANDIDATE_UPDATE, payload: id });
};

export const UPDATE_CANDIDATE = 'UPDATE_CANDIDATE';
export const UPDATED_CANDIDATE = 'UPDATED_CANDIDATE';
export const UPDATE_CANDIDATE_ERROR = 'UPDATE_CANDIDATE_ERROR';

export const updateCandidate = async (dispatch, candidate) => {
    dispatch({ type: UPDATE_CANDIDATE });
    try {
        const response = await axios.put(`/candidates/${candidate.id}`, candidate);
        dispatch({ type: UPDATED_CANDIDATE, payload: response.data });
    } catch (err) {
        dispatch({ type: UPDATE_CANDIDATE_ERROR, payload: extractError(err) });
    }
};

export const DELETE_CANDIDATE = 'DELETE_CANDIDATE';
export const DELETED_CANDIDATE = 'DELETED_CANDIDATE';
export const DELETE_CANDIDATE_ERROR = 'DELETE_CANDIDATE_ERROR';

export const deleteCandidate = async (dispatch, id) => {
    dispatch({ type: DELETE_CANDIDATE });
    try {
        await axios.delete(`/candidates/${id}`);
        dispatch({ type: DELETED_CANDIDATE, payload: id });
    } catch (err) {
        dispatch({ type: DELETE_CANDIDATE_ERROR, payload: extractError(err) });
    }
};
