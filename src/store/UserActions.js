import axios from "axios";
import { add, extractError } from "./AbstractActions";

export const LOAD_USERS = 'LOAD_USERS';
export const LOADED_USERS = 'LOADED_USERS';
export const LOAD_USERS_ERROR = 'LOAD_USERS_ERROR';

export const loadUsers = async dispatch => {
    dispatch({ type: LOAD_USERS });
    try {
        const response = await axios.get('/users');
        dispatch({ type: LOADED_USERS, payload: response.data });
    } catch (err) {
        dispatch({ type: LOAD_USERS_ERROR, payload: extractError(err) })
    }
};

export const ADD_USER = 'ADD_USER';
export const ADDED_USER = 'ADDED_USER';
export const ADD_USER_ERROR = 'ADD_USER_ERROR';

export const addUser = async (dispatch, user) => {
    add(dispatch, user, '/users', [ADD_USER, ADDED_USER, ADD_USER_ERROR]);
};

export const START_USER_UPDATE = 'START_USER_UPDATE';

export const startUserUpdate = (dispatch, user) => {
    dispatch({ type: START_USER_UPDATE, payload: user });
};

export const UPDATE_USER = 'UPDATE_USER';
export const UPDATED_USER = 'UPDATED_USER';
export const UPDATE_USER_ERROR = 'UPDATE_USER_ERROR';

export const updateUser = async (dispatch, user) => {
    dispatch({ type: UPDATE_USER });
    try {
        const response = await axios.put(`/users/${user.id}`, user);
        dispatch({ type: UPDATED_USER, payload: response.data });
    } catch (err) {
        dispatch({ type: UPDATE_USER_ERROR, payload: extractError(err) });
    }
};

export const DELETE_USER = 'DELETE_USER';
export const DELETED_USER = 'DELETED_USER';
export const DELETE_USER_ERROR = 'DELETE_USER_ERROR';

export const deleteUser = async (dispatch, id) => {
    dispatch({ type: DELETE_USER });
    try {
        await axios.delete(`/users/${id}`);
        dispatch({ type: DELETED_USER, payload: id });
    } catch (err) {
        dispatch({ type: DELETE_USER_ERROR, payload: extractError(err) });
    }
};
