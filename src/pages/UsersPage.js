import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import UserForm from "../components/users/user-form/UserForm";
import UserList from "../components/users/user-list/UserList"
import { addUser, deleteUser, loadUsers, startUserUpdate, updateUser } from "../store/UserActions";
import './Layout.css';

const UsersPage = () => {

    const dispatch = useDispatch();
    const message = useSelector(state => state.uiState.error);
    const fetchUsers = useCallback(() => loadUsers(dispatch), [dispatch]);

    useEffect(fetchUsers, [fetchUsers]);

    const handleButtonClick = useCallback(
        user => user?.id ? updateUser(dispatch, user) : addUser(dispatch, user),
        [dispatch]
    );
    const editUser = useCallback(user => startUserUpdate(dispatch, user), [dispatch]);
    const removeUser = useCallback(user => deleteUser(dispatch, user.id), [dispatch]);

    return <div>
        <div className="LeftPane">
            <UserList editUser={editUser} deleteUser={removeUser} />
        </div>
        <div className="RightPane">
            {message && <div style={{color: 'red'}}><h2>{message}</h2></div>}
            <UserForm handleButtonClick={handleButtonClick} />
        </div>
    </div>;
};

export default UsersPage;