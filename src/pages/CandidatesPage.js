import { Route, Switch, useHistory, useRouteMatch } from "react-router-dom";
import CandidateForm from "../components/candidates/candidate-form/CandidateForm";
import CandidateList from "../components/candidates/candidate-list/CandidateList";
import './Layout.css';

import { addCandidate as add, updateCandidate as edit, deleteCandidate, loadCandidates, startCandidateAdd } from '../store/CandidateActions';
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect } from "react";

const CandidatesPage = () => {

    const match = useRouteMatch();
    const { url } = match;
    const history = useHistory();
    const dispatch = useDispatch();

    const message = useSelector(state => state.uiState.error);
    const candidateForEdit = useSelector(state => state.candidatesState.candidateForEdit);

    useEffect(() => loadCandidates(dispatch), [dispatch]);

    const startAddCandidate = useCallback(() => {
        startCandidateAdd(dispatch)
    }, [history, match]);

    useEffect(() => {
        candidateForEdit && !candidateForEdit.id && history.push(`${url}/add`)
    }, [candidateForEdit]);

    useEffect(() => {
        !message && !candidateForEdit && history.push(url)
    }, [message, candidateForEdit, history, url]);

    return <div>
        <Switch>
            <Route path={`${url}/add`}>
                <CandidateForm handleButtonClick={candidate => add(dispatch, candidate)} />
            </Route>
            <Route path={`${url}/:candidateId`}>
                <CandidateForm handleButtonClick={candidate => edit(dispatch, candidate)} />
            </Route>
            <Route path={url}>
                <CandidateList delete={c => deleteCandidate(dispatch, c.id)} />
                <a onClick={startAddCandidate}>Add candidate</a>
            </Route>
        </Switch>
    </div>;
};

export default CandidatesPage;